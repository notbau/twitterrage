library(tm)
library(SnowballC)
library(wordcloud)
library(RColorBrewer)
library(wordcloud2)

filePath_app <- "negsent_app.csv"
text <- readLines(filePath_app)
docs_app<-Corpus(VectorSource(text))
#inspect(docs)
toSpace <- content_transformer(function (x, pattern) gsub(pattern, " ", x))
docs_app <- tm_map(docs_app, toSpace, "/")
docs_app <- tm_map(docs_app, toSpace, "@")
docs_app <- tm_map(docs_app, toSpace, "\\|")

# Convert the text to lower case
docs_app <- tm_map(docs_app, content_transformer(tolower))
# Remove numbers
dapp <- tm_map(docs_app, removeNumbers)
# Remove english common stopwords
docs_app <- tm_map(docs_app, removeWords, stopwords("en"))
# Remove your own stop word
# specify your stopwords as a character vector
docs_app <- tm_map(docs_app, removeWords, c("still", "fees", "told", "sure", "cos", "pay", "via", "many", 
                                              "store", "home", "log", "start", "way", "cant", "due", "far", "using", 
                                              "today", "since", "instead", "right", "night", "used",  "the", "keep", "soon", 
                                              "someone", "hello", "back", "pin", "guy", "want", "never", "will", "mins", "see", 
                                              "know", "ago", "find", "use", "going", "nab", "like", "really", "now", "every", 
                                              "ever", "trying","one", "goes", "says","they", "get", "can", 
                                              "guys", "need", "hey", "got", "bank", "anz", "just","even","make","sent",
                                              "send","buy","hell", "much", "times","give", "days","take","anyone", "yet",
                                              "site", "çös","able","twice","new", "think","minutes","answer", "number",
                                              "please","different", "limit", "year", "banks","hold", "week", "open", "australia"
                                              , "work","details", "good", "made", "address", "getting", "without", "unable", "pending"
                                              , "looking", "already", "change", "tranfers", "weeks", "fee", "line", "last", "apply", "later"
                                              , "paid", "month", "update", "cancel", "team", "machines", "coming", "moving", "things", "hour"
                                              , "tried", "black", "close", "cost", "fix", "day", "travel", "staff", "months", "statement"
                                              , "offer", "support", "happening", "personal", "application", "making", "person", "receive"
                                              , "received", "eta", "help", "less", "anzau", "hard", "cash", "joke", "coin", "else", "closed"
                                              , "message", "next", "password", "cut", "two", "tap", "worst", "fail", "ivanmioc", "shield"
                                              , "working", "services", "needs", "sparkscccc", "stolen", "exchange", "years", "first", "spend"
                                              , "login", "called", "done", "emails", "machine", "email", "payments", "points", "falcon", "contact"
                                              , "transfers", "option", "come", "moment", "point", "let", "view", "gave", "name", "read", "name",
                                              "dont", "ive", "error"
)) 
# Remove punctuations
docs_app <- tm_map(docs_app, removePunctuation)
# Eliminate extra white spaces
docs_app <- tm_map(docs_app, stripWhitespace)
# Text stemming, retrieve root word
#docs <- tm_map(docs, stemDocument)

dtm <- TermDocumentMatrix(docs_app, control=list(wordLengths=c(-Inf, Inf)))
m <- as.matrix(dtm)
v <- sort(rowSums(m),decreasing=TRUE)
d <- data.frame(word=names(v), freq=v)

#search for frequent terms
findFreqTerms(dtm, lowfreq =10)

#show the list of top n number of word
head(d,9)

barplot(d[1:9,]$freq, las = 2, names.arg = d[1:9,]$word, ylim= c(0,70),
        col ="lightblue", main ="App Negative Sentiments",
        ylab = "Words")


