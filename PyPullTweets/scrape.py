#referencea:
#https://stackoverflow.com/questions/29858752/error-message-chromedriver-executable-needs-to-be-available-in-the-path
#https://sites.google.com/a/chromium.org/chromedriver/downloads
#https://github.com/bpb27/twitter_scraping/
#http://tomkdickinson.co.uk/2016/12/extracting-a-larger-twitter-dataset/
#http://adilmoujahid.com/posts/2014/07/twitter-analytics/
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import NoSuchElementException, StaleElementReferenceException
from time import sleep
import json
import datetime


# edit these three variables
user = 'ANZ_AU'
start = datetime.datetime(2017, 10, 1)  # year, month, day # e.g. 2016 10 1
end = datetime.datetime(2017, 10, 5)  # year, month, day e.g. 2016 9 30
#twitter limitation - can't get more than a week's worth of data. Block process day by day instead


delay = 1  # time to wait on each page load before reading the page. BUFFER
driver = webdriver.Chrome("C:/Users/Rentec/Desktop/twitterrage/download/chromedriver_win32/chromedriver.exe")
# options are Chrome() Firefox() Safari()


# don't mess with this stuff
twitter_ids_filename = './data/all_ids.json' #Stores ID numbers of tweets to pull out separately
days = (end - start).days + 1
id_selector = '.time a.tweet-timestamp' #ID when you look at Twitter's DOM for a list of tweets
tweet_selector = 'li.js-stream-item' #The actual tweet when you look at Twitter's DOM for a list of tweets
user = user.lower()
ids = []

def format_day(date): #need date in consistent format for block process
    day = '0' + str(date.day) if len(str(date.day)) == 1 else str(date.day) #prefix 0 to single digit days unless it is double digit
    month = '0' + str(date.month) if len(str(date.month)) == 1 else str(date.month) #same as above for months
    year = str(date.year)
    return '-'.join([year, month, day])

def form_url(since, until):
    url1 = 'https://twitter.com/search?f=tweets&vertical=default&q=to%3A'
    url2 =  user + '%20since%3A' + since + '%20until%3A' + until + '&tweet_mode=extended&in_reply_to_status_id=null&src=typd&lang=en'# 'src=typd&lang=en'#    #+ 'include%3Aretweets&src=typd&lang=en'
    return url1 + url2

def increment_day(date, i):
    return date + datetime.timedelta(days=i) #increment the dates

for day in range(days):
    d1 = format_day(increment_day(start, 0))
    d2 = format_day(increment_day(start, 1))
    url = form_url(d1, d2) # loop1: day 1 to day 2, loop2: day 2 to day 3
    print(url)
    print(d1)
    driver.get(url)
    sleep(delay)

    try: #tweets wont all appear at same time. Need to keep scrolling down to reveal more
        found_tweets = driver.find_elements_by_css_selector(tweet_selector) #get the tweets for the current page from selenium
        increment = 10 #threshold

        while len(found_tweets) >= increment: #if there are more than 10, you probably need to scroll down the page to see the rest of the tweets in the DOM
            print('scrolling down to load more tweets') #since the non-visible tweets wont be in DOM
            driver.execute_script('window.scrollTo(0, document.body.scrollHeight);') #need to scroll to bottom for extra tweets to load
            sleep(delay)
            found_tweets = driver.find_elements_by_css_selector(tweet_selector) #old tweets wont get removed from DOM. ok to overwrite
            increment += 10 #keep needing to scroll down until you get all the tweets for that block process of days

        print('{} tweets found, {} total'.format(len(found_tweets), len(ids)))

        for tweet in found_tweets:
            try:
                id = tweet.find_element_by_css_selector(id_selector).get_attribute('href').split('/')[-1]
                ids.append(id) #collect ids
            except StaleElementReferenceException as e:
                print('lost element reference', tweet)

    except NoSuchElementException:
        print('no tweets on this day')

    start = increment_day(start, 1) #in case of error for particular block process, skip to next one


try:
    with open(twitter_ids_filename) as f:
        all_ids = ids + json.load(f)
        data_to_write = list(set(all_ids)) #set() = set of distinct objects, list() puts everything back in a list format
        print('tweets found on this scrape: ', len(ids))
        print('total tweet count: ', len(data_to_write))
except FileNotFoundError:
    with open(twitter_ids_filename, 'w') as f:
        all_ids = ids
        data_to_write = list(set(all_ids))  #removes dupol
        print('tweets found on this scrape: ', len(ids))
        print('total tweet count: ', len(data_to_write))

with open(twitter_ids_filename, 'w') as outfile:
    json.dump(data_to_write, outfile)

print('all done here')
driver.close()

#https://twitter.com/statuses/ID plug an id into this url to see the tweet
