#https://stackoverflow.com/questions/32382686/unicodeencodeerror-charmap-codec-cant-encode-character-u2010-character-m
import customtweepy  #workaround until fix gets pushed into tweepy github: https://github.com/tweepy/tweepy/pull/926
import json
import math
import glob
import os
import csv
import zipfile
import zlib
from tweepy import TweepError
from time import sleep



with open('api_keys.json') as f: #load credentials for handshake
    keys = json.load(f)

auth = customtweepy.OAuthHandler(keys['consumer_key'], keys['consumer_secret']) #handshake
auth.set_access_token(keys['access_token'], keys['access_token_secret'])
api = customtweepy.API(auth)




user = 'ANZ_AU'
user = user.lower() #filenames
output_file = './data/{}.json'.format(user)
output_file_short = './data/{}_short.json'.format(user)
directory = os.path.dirname('./data/')
if not os.path.exists(directory): #create folder for data
   os.makedirs(directory)



with open('./data/all_ids.json') as f: #load tweet IDs
    ids = json.load(f)

print('total ids: {}'.format(len(ids)))





all_data = []
start = 0 #block process
end = 100
limit = len(ids)
i = math.ceil(limit / 100) #how many loops to iterate for block processing







for go in range(i):
    print('currently getting {} - {}'.format(start, end))
    sleep(6)  # needed to prevent hitting API rate limit
    id_batch = ids[start:end] #batch of ids
    start += 100
    end += 100
    tweets = api.statuses_lookup(id_batch, tweet_mode='extended') #extended tweets returned
    for tweet in tweets:
#        if (dict(tweet._json)["in_reply_to_status_id"] is None):
         all_data.append(dict(tweet._json)) #save tweets from that batch

print('metadata collection complete') #full data store done (for reference purposes), now to create simplified data store that we will use
print('creating master json file')
with open(output_file, 'w') as outfile:
    json.dump(all_data, outfile)








results = []

#def is_retweet(entry):
#    return 'retweeted_status' in entry.keys()

#def get_source(entry):
#    if '<' in entry["source"]:
#        return entry["source"].split('>')[1].split('<')[0]
#    else:
#        return entry["source"]
count = 0
with open(output_file) as json_data: #pull data i want from full file and push to new (short) file
    data = json.load(json_data)
    for entry in data:
        t = {
            "id_str": entry["id_str"],
            "created_at": entry["created_at"], #uncomment when time to scale up project to include other features
            "full_text": entry["full_text"],
            "in_reply_to_status_id": entry["in_reply_to_status_id"]
            # "in_reply_to_screen_name": entry["in_reply_to_screen_name"],
          # "retweet_count": entry["retweet_count"],
          # "favorite_count": entry["favorite_count"],
          #  "source": get_source(entry),
          #  "is_retweet": is_retweet(entry)
        }
  #      if (t["in_reply_to_status_id"]) is None:
   #         results.append(t)
        results.append(t)

print('creating minimized json master file')
with open(output_file_short, 'w') as outfile:
    json.dump(results, outfile)







with open(output_file_short) as master_file: #short json in csv in case csv is easier to analyse
    data = json.load(master_file)
    #fields = ["favorite_count", "source", "full_text", "in_reply_to_screen_name", "is_retweet", "created_at", "retweet_count", "id_str"]
    fields = ["full_text", "created_at", "id_str"]
    print('creating CSV version of minimized json master file')
    f = csv.writer(open('./data/{}.csv'.format(user), 'w',encoding='utf-8', newline=''))
    f.writerow(fields)
    for x in data:
        #f.writerow([x["favorite_count"], x["source"], x["text"], x["in_reply_to_screen_name"], x["is_retweet"], x["created_at"], x["retweet_count"], x["id_str"]])
        f.writerow([   x["id_str"],x["created_at"],x["full_text"],x["in_reply_to_status_id"]    ])
