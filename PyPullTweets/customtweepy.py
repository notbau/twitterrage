import tweepy
from tweepy import *
from tweepy.binder import bind_api
from tweepy.utils import list_to_csv


class API(tweepy.API): #workaround until fix gets pushed into tweepy github: https://github.com/tweepy/tweepy/pull/926


    def statuses_lookup(self, id_, include_entities=None, trim_user=None, map_=None, tweet_mode=None):
        return self._statuses_lookup(
            list_to_csv(id_),
            include_entities,
            trim_user,
            map_,
            tweet_mode
        )

    @property
    def _statuses_lookup(self):
        # maybe params needed to be scpecified
        return bind_api(
            api=self,
            path='/statuses/lookup.json',
            payload_type='status',
            payload_list=True,
            allowed_param=['id', 'include_entities', 'trim_user', 'map', 'tweet_mode'],
            require_auth=True
        )