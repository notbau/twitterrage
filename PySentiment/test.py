import nltk
import pandas as pd
import numpy as np
from matplotlib import pyplot as plt


pd.set_option('display.max_colwidth', -1)
filename = "../PyProcessor/ManualScoreFromProcessedTweets.json"
columnName = ['id_str','created_at', 'full_text','sentiment']
data = pd.DataFrame(pd.read_json(filename, orient=columnName,encoding="utf-8",dtype={'id_str':str}),columns=columnName)

NegData = (data[data['sentiment'].isin(['1','2'])]).sample(frac=1)
PosData = (data[data['sentiment'].isin(['4','5'])]).sample(frac=1)

#print(NegData)

NegTrain=NegData[['full_text','sentiment']].sample(frac=0.8,random_state=200)
NegTest=NegData[['full_text','sentiment']].drop(NegTrain.index)
PosTrain=PosData[['full_text','sentiment']].sample(frac=0.8,random_state=200)
PosTest=PosData[['full_text','sentiment']].drop(PosTrain.index)
#trainNeg, testNeg = train_test_split(NegData, test_size=0.2)
#trainPos, testPos = train_test_split(PosData, test_size=0.2)

#print(NegTrain)
#print(PosTrain)
NegTrain = list(zip(*[NegTrain[c].values.tolist() for c in NegTrain]))
NegTest = list(zip(*[NegTest[c].values.tolist() for c in NegTest]))
PosTrain = list(zip(*[PosTrain[c].values.tolist() for c in PosTrain]))
PosTest = list(zip(*[PosTest[c].values.tolist() for c in PosTest]))


pos_tweets = [('I love this car', 'positive'),
              ('I feel great this morning', 'positive'),
              ('He is my best friend', 'positive')]

neg_tweets = [('I do not like this car', 'negative'),
              ('I feel tired this morning', 'negative'),
              ('He is my enemy', 'negative')]

test_pos_tweets = [('This view is amazing', 'positive'),
              ('I am so excited about the concert', 'positive'),
              ('I had the best day ever', 'positive')]

test_neg_tweets = [('This view is horrible', 'negative'),
              ('I am not looking forward to the concert', 'negative'),
              ('I hated the experience', 'negative')]
#print(test_neg_tweets)

















tweets = []

print(pos_tweets[0])
#test = list(zip(*[PosTest[c].values.tolist() for c in ['full_text', 'sentiment']]))

print(PosTrain[1])

# test_tweets = []
# for (words, sentiment) in pos_tweets + neg_tweets:
#     words_filtered = [e.lower() for e in words.split() if len(e) >= 3]
#     tweets.append((words_filtered, sentiment))
#
# for (words, sentiment) in test_pos_tweets + test_neg_tweets:
#     words_filtered = [e.lower() for e in words.split() if len(e) >= 3]
#     test_tweets.append((words_filtered, sentiment))
#


test_tweets = []
for (words, sentiment) in PosTrain + NegTrain:

    words_filtered = [e.lower() for e in words.split() if len(e) >= 3]
    tweets.append((words_filtered, sentiment))

for (words, sentiment) in PosTest + NegTest:
    words_filtered = [e.lower() for e in words.split() if len(e) >= 3]
    test_tweets.append((words_filtered, sentiment))




print(tweets)
print(test_tweets)


def get_word_features(wordlist):
    wordlist = nltk.FreqDist(wordlist)
    print(wordlist.most_common(5))
    word_features = wordlist.keys()
    return word_features


def get_words_in_tweets(tweets):
    all_words = []
    for (words, sentiment) in tweets:
      all_words.extend(words)
    return all_words

word_features = get_word_features(get_words_in_tweets(tweets))
#print(word_features)

def extract_features(document):
    document_words = set(document)
    features = {}
    for word in word_features:
        features['contains(%s)' % word] = (word in document_words)
    return features

print(extract_features(tweets[0][0]))


training_set = nltk.classify.apply_features(extract_features, tweets)

#print(training_set)

classifier = nltk.NaiveBayesClassifier.train(training_set)


#print(classifier.show_most_informative_features(32))


tweet = 'i paid my credit card last sat and its still not showing up on the transactions last night'
print(classifier.classify(extract_features(tweet.split())))
