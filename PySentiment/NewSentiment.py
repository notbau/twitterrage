import nltk
import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
from nltk import word_tokenize
from nltk.corpus import stopwords
from nltk import FreqDist
from nltk.stem.snowball import SnowballStemmer
import matplotlib.pyplot as plt
import json
import matplotlib.pyplot as plt
import csv
stemmer = SnowballStemmer("english")


pd.set_option('display.max_colwidth', -1)
#filename = "../PyProcessor/ManualScoreWithoutStops.json"
inputManual = "../PyProcessor/ManualScoreWithoutStops1-3.json"
sentiment_answer_short = "./dataOut/answer_short1-3.json"
wrongAnswers = "./dataOut/wrong_answers1-3.json"
columnName = ['id_str', 'full_text','sentiment','created_at']
data = pd.DataFrame(pd.read_json(inputManual, orient=columnName,encoding="utf-8",dtype={'id_str':str}),columns=columnName)
data['bkp_text'] = data['full_text']
data['full_text'] = data['full_text'].apply(lambda x: [stemmer.stem(y) for y in x])

#data = (data[data['sentiment'].isin(['1','2','4','5'])]).sample(frac=1)
data2 = (data[data['sentiment'].isin(['1','3'])]).sample(frac=1)
train=data2[['full_text','sentiment','created_at','bkp_text','id_str']].sample(frac=0.8,random_state=200)
test=data2[['full_text','sentiment','created_at','bkp_text','id_str']].drop(train.index)




def get_all_words(dataframe):
    """ A function that gets all the words from the Joke column in a given dataframe """
    all_words = []
    for tweet in dataframe['full_text']:
        all_words.extend(tweet)
    return all_words


#===================================================================


def get_word_features(wordlist):
    wordlist = nltk.FreqDist(wordlist)
    (print('list',wordlist.keys()))
    word_features = wordlist.keys()
    return word_features

# ==================================================================
#print(train)
#print(train['full_text'])
all_words = get_all_words(data2)  # This uses all jokes before 2013 as our training data set.



fdist = FreqDist(all_words).most_common()
#print(fdist)
#fdist.json.dump('./dataOut/WordFrequencies.json')



def extract_features(theTweet, all_words):
    words = set(theTweet)
    features = {}
    for word in words:
        features['contains(%s)' % word] = (word in all_words)

    return features


train['Features'] = train['full_text'].apply(lambda x:extract_features(x, all_words))





train['Labeled_Feature'] = list(zip(train['Features'],train['sentiment']))
#print(train['Labeled_Feature'])
classifier = nltk.NaiveBayesClassifier.train(train['Labeled_Feature'])

#classifier.show_most_informative_features(10)




stop = set(stopwords.words('english'))
stop.update(["im","hi","anz"])
keepWords = set(['couldn'])
stop = stop - keepWords
#print(stop)





# data['full_text'] = data['full_text'].apply(lambda x: [i for i in word_tokenize(x) if i not in stop]) #remove stopwords
string_to_list = lambda x: [i for i in word_tokenize(x) if i not in stop]


test['answer'] = (test['full_text']).apply(lambda x: classifier.classify(extract_features(x, all_words)) )
test.to_json(sentiment_answer_short,orient='records',date_format='iso')

#print(test[['sentiment','answer']])
hi = test[(test.answer == test.sentiment)]

Fn = len(test[(test["sentiment"] == 1) & (test["answer"] == 3)])
Tp = len(test[(test["sentiment"] == 3) & (test["answer"] == 3)])
Tn = len(test[(test["sentiment"] == 1) & (test["answer"] == 1)])
Fp = len(test[(test["sentiment"] == 3) & (test["answer"] == 1)])
R = Tp/(Tp+Fn)
P= Tp/(Tp+Tn)
print('accuracy = ',len(hi)/len(test))
print ('precision = ',P)
print ('recall = ',R)
print ('F1 = ',2*((R*P)/(R+P)))

#classifier.labels()
#classifier.show_most_informative_features()

test2 = test[(test.answer != test.sentiment)]
test2.to_json(wrongAnswers,orient='records',date_format='iso')



#to send off for data analysis
originalProcData = pd.DataFrame(pd.read_json('C:/Users/Rentec/Desktop/twitterrage/PyProcessor/dataO/processed_short.json', orient=columnName,encoding="utf-8",dtype={'id_str':str}),columns=columnName)
test['bkp_text'] = originalProcData['full_text']
test[['bkp_text','sentiment','answer']].to_csv('./dataOut/outputAnswers.csv', encoding='utf-8')






    #plt.interactive(False)

#plt.hist(test['sentiment'])

#fig, ax = plt.subplots()
#print(data['sentiment'].value_counts()) #1) all scores from my manual scoring +2k
#print(data2['sentiment'].value_counts()) #2)same as 1) but without neutral/irrelevant
#print(test['sentiment'].value_counts()) #3)the number of tweets actually tested
#print(hi['sentiment'].value_counts())    #4)number of tweets from 3) where scores match
#print(test2['sentiment'].value_counts()) #5)number of tweets from 3) where scores do not match




# bins = 3
# plt.hist(data['sentiment'],range=[1, 3],label='x',align='mid')
# plt.hist(test['sentiment'],range=[1, 3],label='y',align='mid')
# plt.legend(loc='upper right')
# plt.show()

#test.plot(x='created_at',y='answer')
#test.set_index('created_at', inplace=True)
#test.groupby('created_at')['answer'].plot(legend=True)

#print( test['created_at'], test['answer'].groupby(test['created_at']) )
#plt.show()




