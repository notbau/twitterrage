import datetime
t1 = datetime.datetime.now()



#https://textblob.readthedocs.io/en/dev/quickstart.html#spelling-correction
#pip3 install pandas
#import ijson
import pandas as pd
from textblob import TextBlob
from nltk.stem import PorterStemmer
import re
from nltk import word_tokenize
#import nltk
#nltk.download('stopwords')
from nltk.corpus import stopwords
import string
pd.set_option('display.max_colwidth', -1)

inputFile = "../PyPullTweets/data/anz_au_short.json"
removeStopWords = "./dataO/wordRemov_short.json"
processed = "./dataO/processed_short.json"
columnName = ['id_str','created_at', 'full_text','in_reply_to_status_id']
#data = pd.read_json(filename, orient=columnName,dtype={'id_str':str})
data = pd.DataFrame(pd.read_json(inputFile, orient=columnName,encoding="utf-8",dtype={'id_str':str}),columns=columnName)
#remove_words = ['@ANZ_AU']
data = data[data['in_reply_to_status_id'].isnull()] #Remove replies
data.drop('in_reply_to_status_id', axis=1, inplace=True)

data = data[data['full_text'].map(len) > 10] #remove messages less than 11 characters long
data['created_at'] = data['created_at'].apply(lambda x: x.date()) #remove some time info - only considering dates for now


#data.to_json('lessTweets_hshort.json',orient='records',date_format='iso')


data['full_text'] = data['full_text'].str.lower() #lower case
data['full_text'] = data['full_text'].replace('^@anz_au(\\u2019s)*[ ,\n\t-]*', '', regex=True) # #anz_au @anz_au (with , or tab, or new line) + same with #@anz_au's
data['full_text'] = data['full_text'].replace('^@anz_nz(\\u2019s)*[ ,\n\t-]*', '', regex=True) # same with @anz_nz's
#data['full_text'] = data['full_text'].replace('^@anz_aus*[ ,\n\t-]*', '', regex=True) # same with @anz_nz's
#data['full_text'] = data['full_text'].replace('^@anz_nzs*[ ,\n\t-]*', '', regex=True) # same with @anz_nz's
data['full_text'] = data['full_text'].replace(' (dm)\b| (DM)\b| (dmed)\b| (DMed)\b', 'direct message', regex=True) #remove twitter dm abbreviation
data['full_text'] = data['full_text'].replace('go money', 'gomoney', regex=True) #remove twitter dm abbreviation
data['full_text'] = data['full_text'].replace('&amp;', ' and ', regex=True) #replace and
data['full_text'] = data['full_text'].replace(' +', ' ', regex=True) #remove multi whitespace
data['full_text'] = data['full_text'].replace('(\\n)', '', regex=True) #replace all tabs with space
data['full_text'] = data['full_text'].replace('(\\t)', '.', regex=True) #replace all new lines with .
data['full_text'] = data['full_text'].replace('(\\u2018)|(\\u2019)', "'", regex=True) #replace apostrophe unicode with '
data['full_text'] = data['full_text'].replace('(\\u201c)|(\\u201d)', "\"", regex=True) #replace double quote unicode with \"
data['full_text'] = data['full_text'].replace('https:\/\/t.co\/(\w{10})', "", regex=True) #remove links (they all seem to follow same format
data['full_text'] = data['full_text'].replace('[^{}]'.format(string.printable), '', regex=True) #weird way to remove rest of unicode???
#data['full_text'] = data['full_text'].str.replace('(.)\1{1,}', '\1\1') #remove extra character haaaappy = haappy
data['full_text'] = data['full_text'].str.replace(' \d\w{2}', '') #remove words like 2nd
data['full_text'] = data['full_text'].str.replace('\d+', '') #remove numbers
data['full_text'] = data['full_text'].map(lambda x: x.strip()) #strip leading/trailing whitespace etc
data['full_text'] = data['full_text'].str.replace('[^\w\s]','') #remove special characters i.e. punctuation ----whats the point of the quotes above then?
data['full_text'] = data['full_text'].replace(' +', ' ', regex=True) #remove multi whitespace
#print(data['full_text'].loc[data['id_str'] == 870177355024683008])
data = data[data['full_text'].map(len) > 10] #remove messages less than 11 characters long
data.to_json(processed,orient='records',date_format='iso')



# def remove_emoji(data):#     try:#         # UCS-4#         patt = re.compile(u'([\U00002600-\U000027BF])|([\U0001f300-\U0001f64F])|([\U0001f680-\U0001f6FF])')#     except re.error:#         # UCS-2#         patt = re.compile(u'([\u2600-\u27BF])|([\uD83C][\uDF00-\uDFFF])|([\uD83D][\uDC00-\uDE4F])|([\uD83D][\uDE80-\uDEFF])')#     patt.sub(u'([\u2600-\u27BF])|([\uD83C][\uDF00-\uDFFF])|([\uD83D][\uDC00-\uDE4F])|([\uD83D][\uDE80-\uDEFF])','',data)#\ud83d\udd11






#==================== SPELL CHECK
#import json
#result = []
# with open( 'processed_short.json') as json_data: #pull data i want from full file and push to new (short) file
#     data = json.load(json_data)
#     for entry in data:
#         #t=TextBlob(entry['full_text'])
#         #result.append(t.correct())
# with open('cor_anz_au_short.json', 'w') as outfile:
#     json.dump(result, outfile)
#data.to_json('correct_anz_au_short.json',orient='records',date_format='iso')

#for i, row in data.iterrows():
 #   data.set_value(i,'full_text',correction(row['full_text']))
#data['full_text'] = data['full_text'].apply(lambda x: TextBlob(x).correct())
#==================== SPELL CHECK






stop = set(stopwords.words('english'))
stop.update(["im","hi"])
#print(stop)
keepWords = set(['couldn'])
stop = stop - keepWords
#print(stop)

data['full_text'] = data['full_text'].apply(lambda x: [i for i in word_tokenize(x) if i not in stop]) #remove stopwords
#print(data['full_text'])



data.to_json(removeStopWords,orient='records',date_format='iso')









t2 = datetime.datetime.now()
# Using Logging to write down calls
print("my_function Execution time: %s" % (t2-t1))
